# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 12:00:10 2020

@author: fysikos6
"""
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
import os
from tqdm import tqdm
import pandas as pd
#from tensorflow.keras import Input
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint
from keras.applications.resnet50 import ResNet50

import keras
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import Concatenate
from keras.layers import Activation
from keras import backend as K
# load model
#model = ResNet50()
# summarize the model
#model.summary()
import os
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from my_classes import DataGenerator
from my_classes import DataGenerator_for_triplets

folder="/media/data/Datasets/food/food/"

def triplet_loss(y_true, y_pred, alpha = 0.4):
    """
    Implementation of the triplet loss function
    Arguments:
    y_true -- true labels, required when you define a loss in Keras, you don't need it in this function.
    y_pred -- python list containing three objects:
            anchor -- the encodings for the anchor data
            positive -- the encodings for the positive data (similar to anchor)
            negative -- the encodings for the negative data (different from anchor)
    Returns:
    loss -- real number, value of the loss
    """
    #print('y_pred.shape = ',y_pred)
    
    total_lenght = y_pred.shape.as_list()[-1]
#     print('total_lenght=',  total_lenght)
#     total_lenght =12
    
    anchor = y_pred[:,0:int(total_lenght*1/3)]
    positive = y_pred[:,int(total_lenght*1/3):int(total_lenght*2/3)]
    negative = y_pred[:,int(total_lenght*2/3):int(total_lenght*3/3)]

    # distance between the anchor and the positive
    pos_dist = K.sum(K.square(anchor-positive),axis=1)

    # distance between the anchor and the negative
    neg_dist = K.sum(K.square(anchor-negative),axis=1)

    # compute loss
    basic_loss = pos_dist-neg_dist+alpha
    loss = K.maximum(basic_loss,0.0)
 
    return loss

def load_images_from_folder(folder): # loads and preprocesses images for inputing
    images = []
    for filename in tqdm(os.listdir(folder)):
        img = load_img(folder + filename, target_size= (224, 224))
        img = img_to_array(img, dtype=int)
        #img = img.reshape((1,) + img.shape)
        if img is not None:
            images.append(img)
    return images

def load_images_from_folder_and_save_as_numpy_arrays(folder): # loads and preprocesses images for inputing
    images = []
    for filename in tqdm(os.listdir(folder)):
        img = load_img(folder + filename, target_size= (224, 224))
        img = img_to_array(img, dtype=int)
        #img = img.reshape((1,) + img.shape)
        filename=filename.replace(".jpg","")
        np.save("/media/data/Datasets/food/food_numpy_arrays/"+filename+'.npy', img)
        
    return print('Saving done')

def from_triplets_to_pairs(dataframe):
    positive_pairs_df=dataframe[[0,1]].copy()
    negative_pairs_df=dataframe[[0,2]].copy()
    
    y_positives=np.ones(positive_pairs_df.shape[0],dtype=int)
    
    negative_pairs_df=negative_pairs_df.rename(columns={2:1})
    
    y_negatives=np.zeros(negative_pairs_df.shape[0],dtype=int)
    
    y_all=np.concatenate((y_positives,y_negatives))
    
    dataframes=[positive_pairs_df,negative_pairs_df]
    full_dataframe=pd.concat(dataframes,ignore_index=True)
    full_array=full_dataframe.to_numpy()
    
    pairs_and_labels=np.concatenate((full_array,y_all.reshape(y_all.shape[0],1)), axis=1)
    return full_array ,y_all, pairs_and_labels


# def images_list_for_x(train_pairs,images):
#     anchorColumn=np.empty(len(train_pairs))
#     comparisonColumn=np.empty(len(train_pairs))
#     for index,row in tqdm(enumerate(train_pairs)):
#         anckor= images[int(train_pairs[index,0])]
#         comparison= images[int(train_pairs[index,1])]
#         anchorColumn=np.append(anchorColumn,anckor)
        
#         comparisonColumn=np.append(comparisonColumn,comparison)
        
#     return anchorColumn, comparisonColumn

def images_list_for_x(train_pairs,images):
    anchorColumn=[]
    comparisonColumn=[]
    for index,row in tqdm(enumerate(train_pairs)):
        anckor= images[int(train_pairs[index,0])]
        comparison= images[int(train_pairs[index,1])]
        anchorColumn.append(anckor)        
        comparisonColumn.append(comparison)      
        
    anchorColumnarray=np.array(anchorColumn)    
    comparisonColumnarray=np.array(comparisonColumn)
    return anchorColumnarray, comparisonColumnarray

def filter_triples(df, maxval):
    k = [df[col].astype(int) < maxval for col in df]
    return df[k[0] & k[1] & k[2]]
   
   
#%%
#Import the txt data
max_img = 10000 #maximum number of images we allow every time

# Load in the data
triples=pd.read_csv('train_triplets.txt',delimiter=" ",dtype=str,header=None)
test_triples=pd.read_csv('test_triplets.txt',delimiter=" ",dtype=str,header=None)
#train_triples, validation_triples=triples.iloc[:53563,:],triples.iloc[53563:,:]


# Truncate
#keep = (triples[0].astype(int) < max_img) & (triples[1].astype(int) < max_img) & (triples[2].astype(int) < max_img)
#print('Keep {} rows'.format(np.sum(keep)))
#triples = triples.loc[keep,:]
triples = filter_triples(triples, max_img)
test_triples = filter_triples(test_triples, max_img)

# Split into test, validation
train_len = int(triples.shape[0]*0.9) # define the percentage of data to be used for training
train_triples, validation_triples=triples.iloc[:train_len,:],triples.iloc[train_len:,:]



#make the triples into 'doubles'
train_pairs , y_train_pairs, train_pairs_and_labels = from_triplets_to_pairs(train_triples)
test_pairs, y_test_pairs, test_pairs_and_labels=from_triplets_to_pairs(test_triples)
validation_pairs, y_validation_pairs, validation_pairs_and_labels= from_triplets_to_pairs(validation_triples)



# #make the triples into 'doubles'
# train_pairs , y_train_pairs = from_triplets_to_pairs(train_triples)
# test_pairs, y_test_pairs=from_triplets_to_pairs(test_triples)
# validation_pairs, y_validation_pairs= from_triplets_to_pairs(validation_triples)

#images=load_images_from_folder("food/food/")

# np.save('all_images.npy', images)
#import all the images
# 
# images=np.load('all_images.npy')

# anchor_list, comparison_list=images_list_for_x(train_pairs,images)
# anchor_list_validation, compraison_list_validation=images_list_for_x(validation_pairs,images)

# anchor_list_test, compraison_list_test= images_list_for_x(test_pairs,images)
 
#%%
input1 = Input((224, 224, 3), name="BackboneNet_input1") # the column with the "main"picture of the pairs?
input2 = Input((224, 224, 3), name="BackboneNet_input2") # the column with the "comparison (related/unrelated, or positive and negative) picture of the pairs?
input3 = Input((224, 224, 3), name="BackboneNet_input3")
# base_model=ResNet50()
backbone = ResNet50(include_top=False, pooling='avg') #.output # what is the backbone?

# for layer in backbone.layers[:-2]:
#     layer.trainable= False

for layer in backbone.layers:
    layer.trainable= False
 
features1 = backbone(input1) # pretrained model input1
features2 = backbone(input2) # pretrained model input2
features3 = backbone(input3) # pretrained model input2



concat = Concatenate()([features1, features2,features3]) # ??
dense = Dense(512)(concat)
dense = Activation('relu')(dense)
dense = Dense(1)(dense )
output = Activation('sigmoid')(dense) 

# define a model with a list of two inputs
model = Model(inputs=[input1, input2, input3], outputs=output) #this Model class describes what..?

# model.summary()
opt = keras.optimizers.Adam(learning_rate=0.0001)
# opt = keras.optimizers.SGD(learning_rate=0.01)
model.compile(optimizer=opt, loss=triplet_loss, metrics=['accuracy']) #this I know
model.summary()

# for layer in backbone.layers:
#     print(layer,layer.trainable)
#%%

# initialize the number of epochs and batch size
 # EPOCHS = 10
# BS = 32 #batch size

# def load_image(filename): # loads and preprocesses images for inputing
    
#     img = load_img(r"food/food/" + filename, target_size= (224, 224)) 
#     img = img_to_array(img, dtype=int)
#         #img = img.reshape((1,) + img.shape)
        
#     return img
            
# def images_on_the_fly(train_pairs,y,batch_size):
#     aug = ImageDataGenerator()
#     genX2 = aug.flow(train_pairs[:,0],train_pairs[:,1], batch_size=batch_size,seed=666)
#     genX1 = aug.flow(train_pairs[:,0],y,  batch_size=batch_size,seed=666)

#     while True:
#             X1i = load_image(genX1.next()+".jpg")
#             X2i = load_image(genX2.next()+".jpg")
#             #Assert arrays are equal - this was for peace of mind, but slows down training
#             #np.testing.assert_array_equal(X1i[0],X2i[0])
#             yield [X1i[0], X2i[1]], X1i[1]
    
      
def gen_flow_for_two_inputs(X1, X2, y, batch_size):
    # construct the training image generator for data augmentation
    aug = ImageDataGenerator()
    genX2 = aug.flow(X1,X2, batch_size=batch_size,seed=2)
    genX1 = aug.flow(X1,y,  batch_size=batch_size,seed=2)
    
    while True:
            X1i = genX1.next()
            X2i = genX2.next()
            #Assert arrays are equal - this was for peace of mind, but slows down training
            #np.testing.assert_array_equal(X1i[0],X2i[0])
            yield [X1i[0], X2i[1]], X1i[1]
            
# gen_flow = gen_flow_for_two_inputs(anchor_list,comparison_list,y_train_pairs,BS)
# validation_flow= gen_flow_for_two_inputs(anchor_list_validation,compraison_list_validation,y_validation_pairs,BS)

# Parameters
EPOCHS = 10
params = {'dim': (224,224),
          'batch_size': 128,
          'n_classes': 2,
          'n_channels': 3,
          'shuffle': True}

# Generators
training_generator = DataGenerator_for_triplets(train_triples,train_pairs_and_labels[:,2], **params)
validation_generator = DataGenerator_for_triplets(validation_triples,validation_pairs_and_labels[:,2], **params)

filepath="./model/weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, mode='max')
callbacks_list = [checkpoint]

H=model.fit_generator(generator=training_generator, validation_data=validation_generator ,epochs=EPOCHS, steps_per_epoch=len(train_pairs_and_labels) //params['batch_size'],validation_steps=len(validation_pairs_and_labels) //params['batch_size'], workers=8, callbacks=callbacks_list)
# H=model.fit_generator(generator=training_generator, validation_data=validation_generator , steps_per_epoch=len(anchor_list,) // BS, validation_steps= len(anchor_list_validation)//BS,epochs=EPOCHS)

model.save("./models/last_model")

#model.fit_generator([anchor_list,comparison_list], y_train_pairs, batch_size=batch_size, epochs=epochs)#, validation_data=(x_test, y_test), shuffle=True) #in the end it should run like this, or?
#%%
#%% Prediction
# y_proba=model.predict_classes(X_test[:]) # here I should predict the result, one or zero from the test data
