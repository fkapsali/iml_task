# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 12:00:10 2020

@author: fysikos6
"""
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
import os
from tqdm import tqdm
import pandas as pd
#from tensorflow.keras import Input
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint
from keras.applications.resnet50 import ResNet50

import keras
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import Concatenate
from keras.layers import Activation
# load model
#model = ResNet50()
# summarize the model
#model.summary()
import os
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from my_classes import DataGenerator

import matplotlib.pyplot as plt



def filter_triples(df, maxval):
    k = [df[col].astype(int) < maxval for col in df]
    return df[k[0] & k[1] & k[2]]


def from_triplets_to_pairs(dataframe):
    positive_pairs_df = dataframe[[0, 1]].copy()
    negative_pairs_df = dataframe[[0, 2]].copy()

    y_positives = np.ones(positive_pairs_df.shape[0], dtype=int)

    negative_pairs_df = negative_pairs_df.rename(columns={2: 1})

    y_negatives = np.zeros(negative_pairs_df.shape[0], dtype=int)

    y_all = np.concatenate((y_positives, y_negatives))

    dataframes = [positive_pairs_df, negative_pairs_df]
    full_dataframe = pd.concat(dataframes, ignore_index=True)
    full_array = full_dataframe.to_numpy()

    pairs_and_labels = np.concatenate((full_array, y_all.reshape(y_all.shape[0], 1)), axis=1)
    return full_array, y_all, pairs_and_labels


max_img = 10000

test_triples= pd.read_csv('test_triplets.txt', delimiter=" ", dtype=str, header=None)

print(np.shape(test_triples))

# test_triplets = test_triples.iloc[0:1000, :]

test_triples = filter_triples(test_triples, max_img)


# make the triples into 'doubles'
test_pairs, y_test_pairs, test_pairs_and_labels = from_triplets_to_pairs(test_triples)


#  # %%
# input1 = Input((224, 224, 3), name="BackboneNet_input1")  # the column with the "main"picture of the pairs?
# input2 = Input((224, 224, 3), name="BackboneNet_input2")  # the column with the "comparison (related/unrelated, or positive and negative) picture of the pairs?
#
model = keras.models.load_model("./model/weights-improvement-03-0.63.hdf5")
# model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])


params = {'dim': (224,224),
          'batch_size': 72,
          'n_classes': 2,
          'n_channels': 3,
          'shuffle': False}

# Generators
test_generator = DataGenerator(test_pairs_and_labels[:,0:2], test_pairs_and_labels[:,2], **params)

print(np.shape(test_pairs_and_labels))

out = model.predict_generator(test_generator, verbose=True)

print(np.shape(out))

out = np.reshape(out, -1)
triplets_num = int(np.shape(out)[0]/2)

print(triplets_num)

with open("test_triplets_out_3.txt", 'w') as file:

    for it_test in range(triplets_num):
        if out[it_test] > out[it_test + triplets_num]:
            file.write("1\n")
        else:
            file.write("0\n")



# for it_sample in range(2*len(test_triples)):
#     input_img = test_generator.__getitem__(it_sample)
#     # plt.imshow(input[0])
#     # plt.show()
#     model.predict_generator(input_img)