# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 11:45:07 2020

@author: fysikos6
"""

import numpy as np
import keras

folder="/media/data/Datasets/food/food_numpy_arrays/"

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, batch_size=32, dim=(224,224), n_channels=3,
                 n_classes=2, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        
    

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        
        list_IDs_temp = [np.r_[self.list_IDs[k],[self.labels[k]]] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp) #gives list with IDs

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        #X = np.empty((self.batch_size, *self.dim, self.n_channels))
        #y = np.empty((self.batch_size), dtype=int)
        X_anchor = np.empty((self.batch_size, *self.dim, self.n_channels))
        X_comparison = np.empty((self.batch_size, *self.dim, self.n_channels))
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
           X_anchor[i,] = np.load(folder + ID[0] + '.npy')
           X_comparison[i,] = np.load(folder + ID[1] + '.npy')
           # Store class
           #print("Heil hitler",ID, len(ID))
           y[i] = ID[2]
           #y[i] = self.list_IDs_temp[i]
           #y[i] = self.list_IDs_temp[i]


        return [X_anchor,X_comparison], y #keras.utils.to_categorical(y, num_classes=self.n_classes) 
    
    
class DataGenerator_for_triplets(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, batch_size=32, dim=(224,224), n_channels=3,
                 n_classes=2, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()
        
    

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        
        list_IDs_temp = [np.r_[self.list_IDs[k],[self.labels[k]]] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp) #gives list with IDs

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        #X = np.empty((self.batch_size, *self.dim, self.n_channels))
        #y = np.empty((self.batch_size), dtype=int)
        X_anchor = np.empty((self.batch_size, *self.dim, self.n_channels))
        X_positive_comparison = np.empty((self.batch_size, *self.dim, self.n_channels))
        X_negative_comparison = np.empty((self.batch_size, *self.dim, self.n_channels))
        #y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
           X_anchor[i,] = np.load(folder + ID[0] + '.npy')
           X_positive_comparison[i,] = np.load(folder + ID[1] + '.npy')
           X_negative_comparison[i,] = np.load(folder + ID[2] + '.npy')
           # Store class
           #print("Heil hitler",ID, len(ID))
           #y[i] = ID[2]
           #y[i] = self.list_IDs_temp[i]
           #y[i] = self.list_IDs_temp[i]


        return [X_anchor,X_positive_comparison,X_negative_comparison]  #keras.utils.to_categorical(y, num_classes=self.n_classes) 