from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from tqdm import tqdm
import os
import numpy as np




def load_images_from_folder_and_save_as_numpy_arrays(folder): # loads and preprocesses images for inputing
    images = []
    for filename in tqdm(os.listdir(folder)):
        img = load_img(folder + filename, target_size= (224, 224))
        img = img_to_array(img, dtype=int)
        #img = img.reshape((1,) + img.shape)
        filename=filename.replace(".jpg","")
        np.save("/media/data/Datasets/food/food_numpy_arrays/"+filename+'.npy', img)

if __name__ == '__main__':
    folder = "/media/data/Datasets/food/food/"
    load_images_from_folder_and_save_as_numpy_arrays(folder)